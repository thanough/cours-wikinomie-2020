---
layout: activity
key: transports-et-mobilite
title: Skyguide
toc:
- "Situation actuelle"
- "Besoins à remplir"
- "Règles de gouvernance"
- "Résultats ou Utilisation"
- "Responsables"
- "Autres acteurs"
- "Recommandations"
tags: ['tag c', 'tag d', 'tag e']
---

### Situation actuelle

La population a à sa disposition une page sur le site internet pour
trouver des informations sur les différents types de vols
disponibles.

### Besoins à remplir

Inciter la population qui n’a pas encore été touché par le
numérique à utiliser cette plateforme pour réaliser les différents
types de vols.

### Règles de gouvernance

Les données des utilisateurs ne sont utilisés que dans le cadre du
processus de recrutement en ligne. L’exploitation de ces données
à des fins publicitaires est exclue.

### Résultats ou Utilisation

En 2019, 96,7% des vols ont été ponctuels. Environ 1'300'000 vols
ont été effectuées. L’effectif monte à 1500 en 2019.

### Responsables

Klaus Meier (Engineering & Technology)

### Autres acteurs

### Recommandations

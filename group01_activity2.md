---
layout: activity
key: transports-et-mobilite
title: TP Publicité
toc:
- "Situation actuelle"
- "Besoins à remplir"
- "Règles de gouvernance"
- "Résultats ou Utilisation"
- "Responsables"
- "Autres acteurs"
- "Recommandations"
tags: ['tag c', 'tag d', 'tag e']
---

### Situation actuelle

La population a à sa disposition une page web et mobile pour
trouver des informations sur les différentes méthodes de
publicités.

### Besoins à remplir

Mettre à disposition des clients, des supports permettant de
garantir l’efficacité optimale dans de la publicité. 

### Règles de gouvernance

Les données confiées par les annonceurs à TP Publicité sont
traitées de manières confidentielles. L’exploitation de ces données
autre qu’à des fins publicitaires est exclue.

### Résultats ou Utilisation

La publicité affichée sur les 1000 écrans embarqués dans les
véhicules des TPG et les 600 distributeurs. 179 millions de
consommateurs touchés.

### Responsables

Tamara Rojas (Coordinatrice de vente)

### Autres acteurs

### Recommandations

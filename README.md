> Notre objectif : 
> Permettre à l’Etat de Genève de situer ses pratiques en matière de Gouvernance numérique dans un contexte international. 
> Nous examinerons les actions et politiques de l’Etat vis-à-vis des technologies en constante évolution, telles que 
> l'intelligence artificielle, les algorithmes, les données ouvertes, les plateformes numériques mais aussi la désinformation, 
> l’accès à l’information, la transparence, la discrimination et la protection de la vie privée, dans les domaines suivants :


* Urbanisme et développement territorial 
* Transports et mobilité
* Santé
* Justice
* Education
* Elections (votations)
* Environnement
* Sécurité - Criminalité
* Médias
* Plateformes
* Sports – Culture